#pragma once

#include "lex_state.h"

namespace Lexer {

// State Handler for Identifier
class id_state : public start_state
{
public:
	// Condition necessary for START of id_state
	static bool match_start(char c)
	{
		return isalpha(c) || c=='_';
	}	

	// Ctor
	id_state(lsm& s)
		: start_state(s)
	{}

	// Debugging Name	
	string name() const { return "id"; }

private:
	// Lexeme involved with the Id
	string lexeme; 

	// Condition necessary for CONTINUATION of id_state
	bool match_continue(char c)
	{
		return match_start(c) || (c >= '0' && c <= '9');
	}

protected:
	// Scan until end of identifier
	virtual int scan(char c) override
	{
		if(match_continue(c))
		{
			lexeme.push_back( c );		
			return (int)lsm::EVENT::CONTINUE;
		}
		else
		{
			LOGF("Id( %s )\n", lexeme.c_str());
			return (int)lsm::EVENT::POP;	
		}
	}
};	

}
