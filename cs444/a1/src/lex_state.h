#pragma once

// istream
#include <fstream>

// Unique Ptr
#include <memory>

// Vector
#include <vector>

// ASSERTF
#include "dev_assert.h"

namespace Lexer {

using buffer_in = std::ifstream;

template<typename T>
using vector = std::vector<T>;

template<typename T>
using uptr = std::unique_ptr<T>;

using string = std::string;

// Abstract Token Interface
class i_token
{
	int					_type_id;
public:
	// Should return a unique ID for this token type
	virtual int 		type_id() const { return _type_id; }

	// Should return a unique Name for this token type
	virtual string 		type_name() const = 0;

	// Used for new identifiers.. Can be empty for symbols
	virtual string		value_name() const = 0;

	using list = vector< i_token* >;

	static list	globals;

	i_token()
	{
		static int id_counter = 0;
		_type_id = id_counter++;
		globals.push_back( this );
	}	
};

// List of Tokens
using token_list = i_token::list;

// Abstract Interface for a single Lexer State
class i_lex_state
{
public:
	virtual int scan(char c) = 0;

	virtual ~i_lex_state() {}

	virtual string name() const = 0;
};

using state_stack = vector< uptr<i_lex_state> >;

// State Machine for Scanning/Lexer Algorithm
struct lex_state_machine
{
	// Input Buffer (Alias'd as std::ifstream)
	buffer_in&		in;

	// Output Token List
	token_list&		out;

	// Stack of States
	state_stack		states;

	// Current Line #
	int				line_no;

	// Ctor
	lex_state_machine(buffer_in& i, token_list& o)
		: in(i)
		, out(o)
		, line_no(0)
	{
	}

	// Returns the top of the state stack
	i_lex_state* top() const { return states.back().get(); }	
	
	// Pushes a new state onto the stack
	void push(i_lex_state* l)
	{
		// LOGF("Pushing new %s state..\n", l->name().c_str());
		states.emplace_back( l );
	}

	// Pops the last state from the stack
	void pop()
	{
		// LOGF("Popping last %s state..\n", states.back().get()->name().c_str());
		states.pop_back();
	}

	// Pushes a new state after allocating it
	template<typename T>
	void push()
	{
		static_assert(std::is_base_of< i_lex_state, T >::value, 
				"Cannot push non-lexical object onto stack");
		push( new T(*this) ); 
	}

	// Response provided by each state to the state machine
	enum EVENT
	{
		PUSH = 0x01,
		POP = 0x02,
		CONTINUE = 0x04
	};

	// Starts the automaton with the starting state.
	void run();
};

// Shorthand Alias for State Machine
using lsm = lex_state_machine;

// List of All States 
class id_state;
class num_literal_state;
class block_cmt_state;
class line_cmt_state;
class symbol_state;
class str_literal_state;
// ---

// Starting state for the LSM
class start_state : public i_lex_state
{
protected:
	// Reference to LSM
	lsm&		machine;

	// Main Scanning algorithm.. ALL CHILDREN MUST IMPLEMENT THIS
	virtual int scan(char c) override;
public:
	// Ctor
	start_state( lsm& m )
		: machine(m)
	{}
	
	// Debugging Name for this state.
	virtual string name() const { return "S"; }
};

// Constructs a starting state and initiates tokenizin
void tokenize(buffer_in& buffer, token_list& tokens_out); 

}
