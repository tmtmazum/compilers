#pragma once

#include "lex_state.h"

// Unordered Map
#include <unordered_map>

// Unordered Set
#include <unordered_set>

namespace Lexer {

template<typename K, typename V>
using hashmap = std::unordered_map<K,V>;

template<typename T>
using hashset = std::unordered_set<T>;

#define ENABLE_ARITHMETIC_OPS 1
#define ENABLE_INCREMENT_OPS 0
#define ENABLE_EQUALITY_OPS 1
#define ENABLE_BOOLEAN_OPS 1
#define ENABLE_EXTRA_ASSIGNMENT_OPS 0

// State handler for symbols
class symbol_state : public start_state
{
public:
	// A token consisting of symbols
	struct symbol : public i_token
	{
		string		lexeme;
		symbol(const char* l)
			: lexeme(l)
		{}

		string type_name() const override { return lexeme; }
		string value_name() const override { return lexeme; }
	};

	// All symbol-related tokens
	struct symbol_table 
	{
		const hashmap< string, symbol >	map = {
			{ "!", "BANG" }
			, { "#", "POUND" }
			, { ".", "DOT" }
			, { ",", "COMMA" }
			, { ";", "SEMICOLON" }
			, { ":", "COLON" }
#if ENABLE_ARITHMETIC_OPS
			, { "+", "PLUS" }
			, { "-", "MINUS" }
			, { "*", "STAR" }
			, { "/", "DIV" }
			, { "%", "PERC" }
#endif

#if ENABLE_INCREMENT_OPS
			, { "++", "INCR" }
			, { "--", "DECR" }
#endif

#if ENABLE_EQUALITY_OPS
			, { "==", "EQ" }
			, { "!=", "NEQ" }
			, { "<=", "LEQ" }
			, { ">=", "GEQ" }
			, { "<", "LT" }
			, { ">", "GT" }
#endif

#if ENABLE_BOOLEAN_OPS
			, { "||", "L_OR" }
			, { "&&", "L_AND" }
			, { "|", "B_OR" }
			, { "&", "B_AND" }
#endif

			, { "(", "LROUND" }
			, { ")", "RROUND" }
			, { "{", "LBRACE" }
			, { "}", "RBRACE" }
			, { "[", "LBRACK" }
			, { "]", "RBRACK" }

			, { "=", "ASSIGN" }
#if ENABLE_EXTRA_ASSIGNMENT_OPS
			, { "+=", "PEQ" }
		    , { "-=", "MEQ" }
			, { "*=", "SEQ" }
			, { "/=", "DEQ" }	
#endif
			};

		// Set of valid character accepted for symbols
		hashset< char >		valid_chars;

		// Ctor
		symbol_table()
		{
			for(auto& m : map)
			{
				const string& s = m.first;
				for(char c : s)
				{
					valid_chars.insert( c );
				}
			}
		}

		// Return true if the character may correspond to a valid symbol
		inline bool is_valid(char c)
		{
			return valid_chars.find(c) != valid_chars.end();
		}

		// Returns true if the string s is a valid symbol
		inline bool exists(const string& s)
		{
			return map.find(s) != map.end();
		}
	};

	// Static singleton for symbol_table
	static symbol_table symbols;

	// Condition necessary for START of id_state
	static bool match_start(char c)
	{
		return symbols.is_valid( c );
	}	
	
	// Ctor
	symbol_state(lsm& s)
		: start_state(s)
		, false_match(false)
	{}

	// Debugging Name
	string name() const { return "symbol"; }

private:
	// Full lexeme for the symbol token
	string 	lexeme;

	// True if this is not a symbol (possibly comment)
	bool	false_match;

	// Condition necessary for CONTINUATION of id_state
	bool match_continue(char c)
	{
		return match_start(c) && (lexeme.size() < 2);
	}

protected:
	// Scan until the end of the symbol
	virtual int scan(char c) override
	{
		if(false_match)
		{
			return (int)lsm::EVENT::POP;
		}
		else if(match_continue(c))
		{
			lexeme.push_back( c );
			if(lexeme.size() == 2)
			{
				if(lexeme.compare("/*")==0)
				{
					machine.push< block_cmt_state >();
					false_match = true;
					return (int)lsm::EVENT::PUSH;					
				}
				else if(lexeme.compare("//")==0)
				{
					machine.push< line_cmt_state >();
					false_match = true;
					return (int)lsm::EVENT::PUSH;					
				}
			}
			return (int)lsm::EVENT::CONTINUE;	
		}
		else
		{
			string first, second;
			// try to match whole string
			if( symbols.exists( lexeme ) )
			{
				LOGF("Symbol(%s)\n", lexeme.c_str());
			}
			else if( symbols.exists( first = lexeme.substr(0, 1) ) )
			{
				LOGF("Symbol(%s)\n", first.c_str());
				LOGF("Symbol(%s)\n", (second = lexeme.substr(1,1)).c_str());
			}
			else
			{
				ASSERTF(false, "Unrecognized Symbol(%s:%c) [Line: %d]\n", 
						lexeme.c_str(), c, machine.line_no);
			}
			return (int) lsm::EVENT::POP;	
		}
	}
}; // End of Symbol State

} // Namespace
