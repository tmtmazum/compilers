#pragma once

#include "lex_state.h"

namespace Lexer {

// State Handler for Line Comments
class line_cmt_state : public start_state
{
public:
	// Condition necessary for START of id_state
	static bool match_start(char c)
	{
		return c=='/';
	}	
	
	// Ctor
	line_cmt_state(lsm& s)
		: start_state(s)
	{}

	// Debugging Name for this state
	string name() const { return "line_comment"; }

private:
	// Condition necessary for CONTINUATION of id_state
	bool match_continue(char c)
	{
		return !(c == '\n');
	}

protected:
	// Scan until end of comment
	virtual int scan(char c) override
	{
		if(match_continue(c))
		{
			return (int) lsm::EVENT::CONTINUE;	
		}
		else
		{
			LOGF("Line Cmt( %s )\n", "??");
			return (int) lsm::EVENT::POP;	
		}
	}

};

// State Handler for Block Comments
class block_cmt_state : public start_state
{
public:
	// Condition necessary for START of id_state
	static bool match_start(char c)
	{
		return c=='*';
	}	

	// Ctor	
	block_cmt_state(lsm& s)
		: start_state(s)
		, last(' ')
	{}

	// Debugging Name
	string name() const { return "block_comment"; }

private:
	// Remember the last character
	char last;

	// Condition necessary for CONTINUATION of id_state
	bool match_continue(char c)
	{
		return !((last == '*') && (c == '/'));
	}

protected:
	// Scan until end of comment
	virtual int scan(char c) override
	{
		if(match_continue(c))
		{
			last = c;
			return (int) lsm::EVENT::CONTINUE;	
		}
		else
		{
			LOGF("Block Comment( ?? )\n");
			return (int) lsm::EVENT::POP;	
		}
	}
};

}
