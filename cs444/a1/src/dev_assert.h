#pragma once

#include <cstdlib>

#define ASSERTF(a, ...) if(!(a)) { fprintf(stdout, __VA_ARGS__); exit(EXIT_FAILURE); }

#define LOGF(...) printf(__VA_ARGS__);
