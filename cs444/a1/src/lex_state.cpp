#include "lex_state.h"

#include "lex_symbol_state.h"
#include "lex_id_state.h"
#include "lex_comment_state.h"
#include "lex_num_literal_state.h"
#include "lex_str_literal_state.h"
#include "dev_assert.h"

namespace Lexer {
	// Static global list of tokens
	i_token::list i_token::globals;

	// Static global list of symbols
	symbol_state::symbol_table symbol_state::symbols;


	void lex_state_machine::run()
	{
		// Push the starting state onto the state machine
		push< start_state >();	
		i_lex_state* _top = top();
		for(char c = in.peek(); in.good() && (c != EOF); c = in.peek())
		{
			ASSERTF(_top, "Top cannot be null, total states=%d.\n",
					states.size());

			// Pass control to current active state to process the character
			EVENT _ev = (EVENT) _top->scan( c );

			if(_ev & EVENT::CONTINUE)
			{
				// LOGF("LSM :: Continue on processing %c\n", c );
				// The active state successfully processed this character
				// - move on to the next.
				in.get();
			}

			if(_ev & EVENT::POP)
			{
				// LOGF("LSM :: Pop on processing %c\n", c );
				// A state requested to be popped off the stack
				pop();
				_top = top();
			}
			else if(_ev & EVENT::PUSH)
			{
				// LOGF("LSM :: Push on processing %c\n", c );
				// A new state was pushed onto the stack
				_top = top();
			}
		}
		// Pop the starting state from the state machine
		pop();
		ASSERTF( states.empty(), 
		"Finished tokenizing but not all states terminated. total states=%d\n", 
			states.size());
	}

	// Primary (Starting) Scanning algorithm
	// Delegates to other handlers for most token types
	// Only processes blanks (' ', '\n')	
	int start_state::scan(char c)
	{
		if(isalpha(c) || c=='_')
		{
			// Delegate to id_state
			machine.push< id_state >();
			return (int)lsm::EVENT::PUSH;			
		}
		else if(isdigit(c))
		{
			// Delegate to num_literal_state
			machine.push< num_literal_state >();
			return (int)lsm::EVENT::PUSH;
		}
		else if(isspace(c) || iscntrl(c))
		{
			// Increment Line #
			if(c=='\n') machine.line_no++;

			// Just process away blank space - return continue
			return (int)lsm::EVENT::CONTINUE;
		}
		else if(c == '"')
		{
			// Delegate to string literal handler
			machine.push< str_literal_state >();
			return (int) lsm::EVENT::PUSH;
		}
		else
		{ // Symbol
			// delegate to symbol state
			machine.push< symbol_state >();
			return (int) lsm::EVENT::PUSH;
		}
	}

	void tokenize(buffer_in& buffer, token_list& tokens_out) 
	{
		lex_state_machine machine(buffer, tokens_out);

		machine.run();
	};
};
