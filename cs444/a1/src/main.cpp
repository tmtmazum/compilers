#include <cstdio>
#include <cassert>
#include <cstdlib>

#include "dev_assert.h"

#define USAGE_MSG "usage : ./joosc.exe <Source-File>\n"

#include "lex_state.h"

int main(int argc, char** argv)
{
	ASSERTF(argc == 2, USAGE_MSG);

	Lexer::buffer_in buf( argv[1] );

	Lexer::token_list tokens;

	Lexer::tokenize( buf, tokens );

	printf("JOBS DONE\n");
	return 0;
}
